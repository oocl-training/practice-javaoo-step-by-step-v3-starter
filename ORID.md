## ORID

### O (Objective)
What did we learn today? What activities did you do? What scenes have impressed you?
```
1. Code review

    This morning, we conducted a Code Review, which mainly pointed out the non-standard variable names in other people's code.

2. Stream API

    The Stream API is a series of stream processing methods that can greatly simplify code with loops.

3. OOP

    Write code to experience the role of polymorphism.

```
### R (Reflective)
Please use one word to express your feelings about today's class.
```
full
```
### I (Interpretive)
What do you think about this? What was the most meaningful aspect of this activity?
```
1. Code review

    Naming in the code is a very important thing in the development process, and it is required that naming can be directly interpreted and understood by others.

    Code review has three main functions: identifying potential bugs in the code, sharing knowledge, and improving the quality and standardization of the team's code.

2. Stream API

    It is not difficult to use the Stream API individually, but how to combine these APIs to achieve a specific problem, such as calculating averages, grouping, and comparing sizes, can test programmers' proficiency in the Stream API.

    The Stream API uses a large number of functional interfaces as formal parameters. These formal parameters can be implemented using lambda expressions for functional interfaces, or they can be directly transferred using newly created objects. If you use objects, you need to use anonymous Inner class.

3. OOP

    Due to my previous studies in C++, I am familiar with the concept and underlying implementation of polymorphism, and can also better understand this lesson.

    Polymorphism is not limited to rewriting, overloading is also a static polymorphism. Dynamic polymorphism, that is, rewriting, is mainly implemented through Virtual function tables in C++.
```
### D (Decisional)
Where do you most want to apply what you have learned today? What changes will you make?
```
    Code Review is a process that we can learn each other and fix bug.

    When we want to simplify our code, we can use Stream API.

    When we want to abstract reality into code, we need the thought of OOP.

    Communicate with others about code, search for their problems, and learn good writing techniques in their code.

    Try to use the Stream API to simplify loop statements in the code as much as possible.
```