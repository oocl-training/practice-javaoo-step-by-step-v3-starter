package ooss;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private final List<Klass> klasses;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        klasses = new ArrayList<>();
    }

    @Override
    public String introduce() {
        if (klasses.isEmpty()) {
            return super.introduce() + " I am a teacher.";
        }
        else {
            String classes = klasses.stream()
                    .map(item -> String.valueOf(item.getNumber()))
                    .sorted(Comparator.comparing(String::valueOf))
                    .collect(Collectors.joining(", "));
            return super.introduce() + " I am a teacher. I teach Class " + classes + ".";
        }

    }

    public void assignTo(Klass klass) {
        if (!klasses.contains(klass)) {
            klasses.add(klass);
        }
    }

    public boolean belongsTo(Klass klass) {
        return klasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return belongsTo(student.getKlass());
    }

    @Override
    public void say(Klass klass) {
        System.out.printf(
                "I am %s, teacher of Class %d. I know %s become Leader.%n",
                name,
                klass.getNumber(),
                klass.getLeader().name
        );
    }
}
