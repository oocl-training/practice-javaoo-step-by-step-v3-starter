package ooss;

public class Student extends Person {
    public Klass getKlass() {
        return klass;
    }

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        if (klass == null) {
            return super.introduce() + " I am a student.";
        } else if (this.equals(klass.getLeader())) {
            return super.introduce() + String.format(
                    " I am a student. I am the leader of class %d.",
                    klass.getNumber()
            );
        } else {
            return super.introduce() + String.format(
                    " I am a student. I am in class %d.",
                    klass.getNumber()
            );
        }
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return klass.equals(this.klass);
    }

    @Override
    public void say(Klass klass) {
        System.out.printf(
                "I am %s, student of Class %d. I know %s become Leader.%n",
                name,
                klass.getNumber(),
                klass.getLeader().name
        );
    }
}
