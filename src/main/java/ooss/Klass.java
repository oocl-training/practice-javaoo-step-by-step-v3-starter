package ooss;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private final int number;

    private final List<Person> peopleAttached;//complete observer pattern


    private Student leader;

    public Klass(int number) {
        peopleAttached = new LinkedList<>();
        this.number = number;
    }

    public Student getLeader() {
        return leader;
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student) {
        if (this.equals(student.getKlass())) {
            leader = student;
            for (Person person : peopleAttached) {
                person.say(this);
            }
        } else {
            System.out.println("It is not one of us.");
        }
    }

    public boolean isLeader(Student student) {
        //在没有设置leader前，这里的leader可能为null，用Objects.equals()方法可避免
        return Objects.equals(leader, student);
    }

    public void attach(Person person) {
        if(!peopleAttached.contains(person))
            peopleAttached.add(person);
    }

    @Override
    public int hashCode() {
        return number;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Klass) {
            return this.hashCode() == obj.hashCode();
        }
        return false;
    }
}
